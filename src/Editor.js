import React, { useState } from 'react';
import styled from 'styled-components';
import RichEditor from './components/Editor/index';

const EditorActionsRow = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

const EditorTitle = styled.h1`
  font-size: 24px;
  font-weight: 100;
  margin: 0 !important;
`

const ActionButtons = styled.div`
  display: flex;
  flex-direction: row;
  margin: 0;
`

const ActionButton = styled.button.attrs({
    type: "button"
  })`
    margin: 0 5px;
    padding: 4px 10px;
    &&:before{
      font-size: 19px;
      line-height: 1;
    }
    &:hover{
      border-color: transparent;
      cursor: pointer;
    }
    :disabled{
        cursor: not-allowed;
    }
}
`;

const Editor = () => {
    const [items, setItems] = useState([
        {
            id: 0,
            value: Math.random() * 100
          }
    ]);
    const [active, setActive] = useState(1);
    const addItem = () => {
        setItems([
            ...items,
            {
              id: items.length,
              value: Math.random() * 100
            }
          ]);
    }
    const nextItem = () => {
        setActive(active+1);
    }
    const prevItem = () => {
        setActive(active-1);
    }

    return (
    <>
        <EditorActionsRow>
            <EditorTitle>Showing Box {active} of {items.length}</EditorTitle>
            <ActionButtons>
                <ActionButton className="fa fa-plus"  onClick={addItem}/>
                <ActionButton className="fa fa-chevron-up"  onClick={prevItem} disabled={active === 1}/>
                <ActionButton className="fa fa-chevron-down"  onClick={nextItem} disabled={active === items.length}/>
            </ActionButtons>
        </EditorActionsRow>
        {
            items.map((item, i) => <RichEditor key={i} show={active === i+1} />)
        }
    </>
    )
};

export default Editor;