import React, { useState, useRef } from 'react'
import { Editor, EditorState, RichUtils,
} from 'draft-js';
import styled from 'styled-components';
import './editor.css';
import BlockStyleControls from './BlockStyleControls';
import InlineStyleControls  from './InlineStyleControls';

const RootWrapper = styled.div`
`;

const EditorWrapper = styled.div`
`;

const styleMap = {
    CODE: {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
      fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
      fontSize: 16,
      padding: 2,
    },
  };
  
  function getBlockStyle(block) {
    switch (block.getType()) {
      case 'blockquote': return 'RichEditor-blockquote';
      default: return null;
    }
  }


const RichEditor = ({show}) => {

  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const editorRef = useRef();

  const focus = () => editorRef.current.editor.focus();

  const onChange = (editorState) => setEditorState(editorState);

  const handleKeyCommand = (command) => {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      return true;
    }
    return false;
  }

  const onTab = (e) => {
    const maxDepth = 4;
    onChange(RichUtils.onTab(e, editorState, maxDepth));
  }

  const toggleBlockType = (blockType) => {
    onChange(
      RichUtils.toggleBlockType(
        editorState,
        blockType
      )
    );
  }

  const toggleInlineStyle = (inlineStyle) => {
    onChange(
      RichUtils.toggleInlineStyle(
        editorState,
        inlineStyle
      )
    );
  }

  // If the user changes block type before entering any text, we can
  // either style the placeholder or hide it. Let's just hide it now.
  let className = 'RichEditor-editor';
  var contentState = editorState.getCurrentContent();
  if (!contentState.hasText()) {
    if (contentState.getBlockMap().first().getType() !== 'unstyled') {
      className += ' RichEditor-hidePlaceholder';
    }
  }

  return (
    <RootWrapper className={`RichEditor-root ${show ? '' : 'hide'}`}>
      <BlockStyleControls
        editorState={editorState}
        onToggle={toggleBlockType}
      />
      <InlineStyleControls
        editorState={editorState}
        onToggle={toggleInlineStyle}
      />
      <EditorWrapper className={className} onClick={focus}>
        <Editor
          blockStyleFn={getBlockStyle}
          customStyleMap={styleMap}
          editorState={editorState}
          handleKeyCommand={handleKeyCommand}
          onChange={onChange}
          onTab={onTab}
          placeholder="Type something..."
          ref={editorRef}
          spellCheck={true}
        />
      </EditorWrapper>
      {/* {console.log(convertToRaw(editorState.getCurrentContent()))}
      <textarea
        disabled
        value={JSON.stringify(convertToRaw(editorState.getCurrentContent()))}
        style={{
          margin: "0px",
          width: "100%",
          height: "200px",
        }}
      /> */}
    </RootWrapper>
  );
}

export default RichEditor;